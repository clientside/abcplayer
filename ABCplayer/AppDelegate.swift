//
//  ABCplayer
//
//  Copyright © 2017 Clientside. All rights reserved.
//

import UIKit
import AVKit


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let avSession = AVAudioSession.sharedInstance()
        try? avSession.setCategory(AVAudioSession.Category(rawValue: convertFromAVAudioSessionCategory(AVAudioSession.Category.playback)), mode: AVAudioSession.Mode(rawValue: convertFromAVAudioSessionMode(AVAudioSession.Mode.default)), options: [])
        try? avSession.setActive(true)

        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        Pulse.shared.stop()
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        Pulse.shared.stop()
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        Pulse.shared.start()
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        Pulse.shared.start()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        Pulse.shared.stop()
    }


}


// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromAVAudioSessionCategory(_ input: AVAudioSession.Category) -> String {
	return input.rawValue
}

// Helper function inserted by Swift 4.2 migrator.
fileprivate func convertFromAVAudioSessionMode(_ input: AVAudioSession.Mode) -> String {
	return input.rawValue
}
