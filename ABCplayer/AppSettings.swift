//
//  ABCplayer
//
//  Copyright © 2017 Clientside. All rights reserved.
//

import Foundation


// Global Constants

/// Set to `false` to prevent the appleTV from going to
/// sleep (as per your appleTV settings) when playing audio
/// -- When the app is in the background ie. screensaver is running then it
///    stops pinging the server for updated JSON until it is in foreground again
let allowSytemToSleep = true

/// Estimated time delay which accounts for buffering etc.
/// -- Even a broken clock is right twice a day
let streamLatency = TimeInterval(40)

// Timeout
let timeOutMinutes = 3.0
let timeOutSeconds = timeOutMinutes * 60.0

