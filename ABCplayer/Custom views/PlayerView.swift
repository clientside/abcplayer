//
//  ABCplayer
//
//  Copyright © 2017 Clientside. All rights reserved.
//

import UIKit
import AVKit

class PlayerView: UIView {
    @IBOutlet private weak var backgroundView: UIView!
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet private weak var albumLabel: UILabel!
    @IBOutlet private weak var artistLabel: UILabel!
    @IBOutlet private weak var trackNameLabel: UILabel!
    @IBOutlet private weak var infoView: UIStackView!
    @IBOutlet private weak var otherInfoLabel: UILabel!
    @IBOutlet private weak var artworkImageView: UIImageView!
    
    private let swipDownRecognizer = UISwipeGestureRecognizer()
    private let swipUpRecognizer = UISwipeGestureRecognizer()
    private var routePickerView: AVRoutePickerView!
    
    private var image: UIImage? {
        didSet {
            artworkImageView.image = image?.roundCorners()
            backgroundImageView.image = image
        }
    }
    
    // MARK: - Custom player setup
    var player: AVPlayer? {
        get {
            return playerLayer.player
        }
        set {
            playerLayer.player = newValue
        }
    }
    
    private var playerLayer: AVPlayerLayer {
        return layer as! AVPlayerLayer
    }
    
    override static var layerClass: AnyClass {
        return AVPlayerLayer.self
    }
    
    // MARK: - View Init
    override func awakeFromNib() {
        super.awakeFromNib()
        swipDownRecognizer.addTarget(self, action: #selector(toggleRoutePicker(swipe:)))
        swipDownRecognizer.direction = .down
        swipUpRecognizer.addTarget(self, action: #selector(toggleRoutePicker(swipe:)))
        swipUpRecognizer.direction = .up
        self.addGestureRecognizer(swipDownRecognizer)
        self.addGestureRecognizer(swipUpRecognizer)
        setupViews()
    }
    
    override func didMoveToSuperview() {
        UIView.animate(withDuration: 0.75, delay: 0, options: [.curveEaseOut], animations: {
            self.backgroundImageView.alpha = 1
        }, completion: { success in
            //
        })
    }
    
    @objc private func toggleRoutePicker(swipe: UISwipeGestureRecognizer) {
        if routePickerView == nil {
            addPicker()
        }
        switch swipe.direction {
        case .down:
            routePickerView.isHidden = false
        case .up:
            routePickerView.isHidden = true
        default:
            return
        }
    }
    
    private func addPicker() {
        let sagOrigin = self.safeAreaLayoutGuide.layoutFrame
        let pickerHeight = CGFloat(58)
        routePickerView = AVRoutePickerView(frame: CGRect(x: sagOrigin.midX - (pickerHeight / 2), y: sagOrigin.origin.y, width: pickerHeight, height: pickerHeight))
        routePickerView.routePickerButtonStyle = .system
        routePickerView.isHidden = true
        routePickerView.clipsToBounds = true
        routePickerView.layer.cornerRadius = CGFloat(pickerHeight / 2)
        self.addSubview(routePickerView)
    }
    
    private func setupViews() {
        backgroundColor = UIColor.black
        backgroundView?.backgroundColor = UIColor.black
        backgroundImageView?.image = nil
        backgroundImageView.alpha = 0
        artworkImageView?.image = nil
        albumLabel?.text = nil
        artistLabel?.text = nil
        trackNameLabel?.text = nil
        otherInfoLabel?.text = nil
    }
    
    // MARK: - View updating
    var isInitialUpdate = true
    func update(withNowPlayingingInfo info: NowPlayingInfo) {
        
        if image != nil {
            let snapshotView = self.snapshotView(afterScreenUpdates: true)
            if snapshotView != nil { self.addSubview(snapshotView!) }
            
            UIView.animate(withDuration: 2.0 , delay: 0, options: [.transitionCrossDissolve] , animations: {
                snapshotView?.alpha = 0
            }) { finished in
                snapshotView?.removeFromSuperview()
            }
        }
        
        image = info.displayArt
        albumLabel.text = info.albumOrServiceName
        artistLabel.text = info.artistOrPresenterOrProgram
        trackNameLabel.text = info.trackOrProgramDescriptionOrServiceDescription
        otherInfoLabel.text = info.durationString
    }
    
}

