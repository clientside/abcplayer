//
//  ABCplayer
//
//  Copyright © 2017 Clientside. All rights reserved.
//

import UIKit
import AVKit

class NowPlayingViewController: UIViewController {
    
    @IBOutlet var playerView: PlayerView!
    var service: Service!

    private(set) lazy var nowPlayingInfo: NowPlayingInfo = NowPlayingInfo(service: service)

    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Reset the Calculated Timing Offset
        TrackUpdateOperation.calculatedTimingOffset = streamLatency
        play()
        NotificationCenter.default.addObserver(self, selector: #selector(checkPulse), name: PulseNotificationName, object: Pulse.shared)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Prevent system sleep when audio is playing
        UIApplication.shared.isIdleTimerDisabled = !allowSytemToSleep
    }

    override func viewWillDisappear(_ animated: Bool) {
        trackUpadateQueue.cancelAllOperations()
        UIApplication.shared.isIdleTimerDisabled = allowSytemToSleep
        service.isAwaitingUpdate = false
        super.viewWillDisappear(animated)
    }
    
    // MARK: - Pulse taking
    @objc func checkPulse(notification: Notification) {
        // Ignore if a TrackUpdateOperation is executing or
        // the Service doesn't provide track information.
        //
        guard !isUpdating, service.hasTrackData else { return }

        // Update Playing Track info if required
        if let shouldUpdateTrack = nowPlayingInfo.trackEvents?.shouldUpdateTrack, shouldUpdateTrack {
            updateTrack()
        }
    }

    private func play() {
        if let url = service.streamUrl {

            let player = AVPlayer(url: url)
            playerView.player = player
            
            if service.hasTrackData {
                updateTrack()
            }
            else {
                playerView.update(withNowPlayingingInfo: nowPlayingInfo)
            }
            
            playerView.player?.play()
        }
    }

    // MARK: - Track updating rituals
    
    let trackUpadateQueue: OperationQueue = {
        let queue = OperationQueue()
        queue.name = "Track updating queue"
        queue.maxConcurrentOperationCount = 1
        return queue
    }()
    
    var isUpdating = false

    private func updateTrack() {
        isUpdating = true
        trackUpadateQueue.cancelAllOperations()

        let op = TrackUpdateOperation(service: service, playingNow: self.nowPlayingInfo.playingNow)
        op.queuePriority = .normal
        op.qualityOfService = .utility
        op.completionBlock = {
            DispatchQueue.main.async {
                if op.isCancelled {
                    // UpdateTrack operation returned in cancelled state
                    // temporarily update the UI with Service info
                    let tempPlayingInfo = NowPlayingInfo(service: self.service)
                    self.playerView.update(withNowPlayingingInfo: tempPlayingInfo)

                    // Reschedule update
                    switch op.state {
                    case .unviable:
                        // Track updating will stop for 5 mins then try again
                        self.rescheduleUpdates(withDelay: 300.0)
                    case .missingTrack:
                        // Track updating will stop for 1.5 min then try again
                        self.rescheduleUpdates(withDelay: 90.0)
                    default:
                        break
                    }

                    self.isUpdating = false
                    return
                }

                // UpdateTrack operation returned in finished state -- update UI
                self.nowPlayingInfo = op.nowPlayingInfo
                self.playerView.update(withNowPlayingingInfo:  op.nowPlayingInfo)
                self.isUpdating = false
            }
        }
        trackUpadateQueue.addOperation(op)
    }

   private func rescheduleUpdates(withDelay delay: TimeInterval) {
        TrackUpdateOperation.calculatedTimingOffset = 0
        do {
            try nowPlayingInfo.setNextUpdate(for: Date().addingTimeInterval(delay))
        } catch {
            // This usually fails because the player starts up and there is no track info
            // available when there should be. -- Kickstart again after 90 seconds
            rebootUpdating(afterDelay: 90.0)
        }
    }

    private func rebootUpdating(afterDelay delay: TimeInterval) {
        Timer.scheduledTimer(withTimeInterval: delay, repeats: false) { [weak self] timer in
            self?.updateTrack()
        }
    }

    // MARK: - Method called from the presenting VC
    func updateProgram() {
        trackUpadateQueue.cancelAllOperations()
        nowPlayingInfo = NowPlayingInfo(service: service)
        playerView.update(withNowPlayingingInfo: nowPlayingInfo)
       rescheduleUpdates(withDelay: 300)
    }

}
