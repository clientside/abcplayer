//
//  ABCplayer
//
//  Copyright © 2017 Clientside. All rights reserved.
//

import UIKit
import AVKit

class ServiceDetailViewController: UIViewController {
    @IBOutlet private weak var imageView: UIImageView!
    @IBOutlet private weak var stationLabel: UILabel!
    @IBOutlet private weak var showLabel: UILabel!
    @IBOutlet private weak var presenterLabel: UILabel!
    @IBOutlet private weak var desctriptionLabel: UILabel!
    @IBOutlet private weak var startEndTimeLabel: UILabel!

    var service: Service!

    // MARK: - ViewController lifecycle methods
    override func viewDidLoad() {
        super.viewDidLoad()
        updateService()
        NotificationCenter.default.addObserver(self, selector: #selector(checkPulse), name: PulseNotificationName, object: Pulse.shared)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Check to see if the Program info is out of date
        if let ttup = timeToUpdateProgram, ttup < Date() {
            updateService()
        }
    }

    // MARK: - Pulse taking
    private var timeToUpdateProgram: Date? {
        let end = service.requestedUpdate ?? service.currentProgram?.endDate ?? service.nextProgram?.startDate ?? service.nextStreamUpdate
        return end?.addingTimeInterval(streamLatency) ?? nil
    }

    @objc func checkPulse(notification: Notification) {
        // Update Current Program if required
        if let ttup = timeToUpdateProgram, ttup < Date() {
            updateService()
        }
    }

    let programUpdateQueue: OperationQueue = {
        let queue = OperationQueue()
        queue.name = "Program updating queue"
        queue.maxConcurrentOperationCount = 1
        return queue
    }()

    private func updateService() {
        programUpdateQueue.cancelAllOperations()
        service.requestedUpdate = nil
        let op = ProgramUpdateOperation(service: service)
        op.queuePriority = .normal
        op.qualityOfService = .utility
        op.completionBlock = {
            guard !op.isCancelled else { return }
            DispatchQueue.main.async {
                switch op.state {
                case .temporary:
                    self.updateDisplay()
                    self.updatePlayer()
                    self.service.requestedUpdate = Date().addingTimeInterval(op.delay)
                case .deferred:
                    self.service.requestedUpdate = Date().addingTimeInterval(60)
                case .final:
                    self.updateDisplay()
                    if self.service.isAwaitingUpdate {
                        self.service.isAwaitingUpdate = false
                    }
                    else {
                        self.updatePlayer()
                    }
                default:
                    break
                }
            }
        }
        programUpdateQueue.addOperation(op)
    }

    // MARK: - Play
    @IBAction func play(_ sender: Any) {
        performSegue(withIdentifier: "playerSegue", sender: nil)
    }
    
    // MARK: - View updating ritual
    private func updateDisplay() {
        programUpdateQueue.cancelAllOperations()
        let playingInfo = NowPlayingInfo(service: service)

        imageView.image =  playingInfo.displayArt
        stationLabel.text = playingInfo.serviceName
        showLabel.text = playingInfo.programTitle
        presenterLabel.text = playingInfo.presenter
        desctriptionLabel.text = playingInfo.programDescription ?? playingInfo.serviceDescription
        startEndTimeLabel.text = playingInfo.programSchedule
    }

    func updatePlayer() {
        if let playerVC = self.presentedViewController as? NowPlayingViewController {
            playerVC.updateProgram()
        }
    }

    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == "playerSegue" else { return }
        if let dvc = segue.destination as? NowPlayingViewController, service != nil {
            dvc.service = service
        }
    }

}

