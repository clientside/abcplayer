//
//  ABCplayer
//
//  Copyright © 2017 Clientside. All rights reserved.
//

import UIKit

class ServicesViewController: UICollectionViewController {
    
    private let reuseIdentifier = "serviceCell"
    @IBOutlet weak var spinner: UIActivityIndicatorView!

    var services: [ABCJSON.Service]? {
        didSet {
            spinner.stopAnimating()
            self.collectionView?.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        /// Register custom cell class
        self.collectionView!.register(UINib(nibName: "ServiceCell", bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
        
        /// Populate dataSource
        let endpoint = Endpoint.Services()
        Endpoint.fetchJSON(url: endpoint.url, type: endpoint.type) { json in
            self.services = json.items
        }
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return services?.count ?? 0
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath) as! ServiceCell
        cell.title = ""
        cell.imageView.image = nil
        
        if let service = services?[indexPath.row] {
            DispatchQueue.global(qos: .userInteractive).async {
                let supportedImages = SupportedImages(title: service.title, primaryImage: service.primaryImage)
                let art = supportedImages.servicesDisplayImage
                
                DispatchQueue.main.async {
                    cell.title  = service.title
                    cell.imageView.image = art.roundCorners()
                    cell.backgroundColor = UIColor.clear
                    }
            }
        }
        
        return cell
    }
   
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if (kind == UICollectionView.elementKindSectionHeader) {
            return collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "servicesHeaderView", for: indexPath)
        }
        return UICollectionReusableView()
    }
    
    // MARK: - Collection view delegate methods
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let service = services?[indexPath.row] {
            performSegue(withIdentifier: "stationInfoSegue", sender: service)
        }
    }
    
    // MARK: - Segue
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == "stationInfoSegue" else { return }
        if let dvc = segue.destination as? ServiceDetailViewController, let service = sender as? ABCJSON.Service {
            dvc.service = Service(service: service)
        }
    }

}
