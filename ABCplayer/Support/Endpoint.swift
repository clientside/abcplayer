//
//  ABCplayer
//
//  Copyright © 2017 Clientside. All rights reserved.
//

import Foundation

struct Endpoint {

    /// Constructs the endpoint `URL` for the JSON which
    /// contains information about the current Show or Program
    /// and provides the `Type` for the JSON request
    ///
    struct Live {
        private let queryString: String
        private var urlString: String {
            return "http://program.abcradio.net.au/api/v1/programitems/\(self.queryString)/live.json?include=next%2Cwith_images%2Cresized_images"
        }

        var url: URL? {
            return URL(string: urlString)
        }

        let type = ABCJSON.Stream.self

        /// serviceID obtained previously from Services JSON
        init(serviceID id: String) {
            self.queryString = id.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        }
    }

    /// Constructs the endpoint `URL` for JSON which
    /// contains information about the currently playing track
    /// and provides the `Type` for the JSON request
    ///
    /// - important: A Service provides a `hasTrackInfo` parameter indicating
    /// whether the Service provides this JSON - **Check that the Service
    /// provides this information before making a request**
    ///
    struct Now {
        private let queryString: String
        private var urlString: String {
            return "http://music.abcradio.net.au/api/v1/plays/\(self.queryString)/now.json"
        }

        var url: URL? {
            return URL(string: urlString)
        }

        let type = ABCJSON.Tracks.self

        /// `serviceID` obtained previously from Services JSON
        init(serviceID id: String) {
            self.queryString = id.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
        }

        private init() {
            self.queryString = "error"
        }
    }

    /// Provides the endpoint `URL` for JSON
    /// and the `Type` for the JSON request
    /// which containts an array of available audio streams
    ///
    struct Services {
        private let urlString = "http://program.abcradio.net.au/api/v1/services.json?filter=has_live_stream&include=primary_image%2Cprimary_webpage%2Coutlets%2Coutlet_audio%2Csocial_media%2Cresized_images%2Ccolours%2Cimages&image_role=background&client_version=1.8.0"

        let type = ABCJSON.Services.self

        var url: URL! {
            return URL(string: urlString)
        }
    }

    // User Agent string is sent in the header of each fetch request
    static let userAgentString = "ABCPlayer.app; Apple tvOS; Version 1.0; (https://bitbucket.org/clientside/abcplayer)"

    /// Requests JSON
    ///
    /// - parameter url: URL which is the endpoint for a JSON request
    /// - parameter type: The type, in this case the type of struct, which is the root element of the JSON
    /// - parameter completion: A block to run at compltion of a succseful URLSession task
    ///
    /// - todo: Implement some error handling!
    ///
    static func fetchJSON<T:Decodable>(url: URL?, type: T.Type, completion: @escaping ((T) -> ())) {
        if let url = url {

            let sessionConfiguration = URLSessionConfiguration.ephemeral
            sessionConfiguration.httpAdditionalHeaders = ["User-Agent": userAgentString]

            let session = URLSession(configuration: sessionConfiguration)
            let task = session.dataTask(with: url) { (data, response, error) in
                guard error == nil else {
                    print(error!)
                    return
                }
                guard let data = data else {
                    print("Data is empty")
                    return
                }

                do {
                    let json = try JSONDecoder().decode(type, from: data)
                    DispatchQueue.main.async {
                        completion(json)
                    }
                } catch let error {
                    print(error)
                }
            }
            task.resume()
        }
    }
}
