//
//  ABCplayer
//
//  Copyright © 2017 Clientside. All rights reserved.
//

import UIKit

// "Power tends to corrupt, and absolute power corrupts absolutely. Great men are almost always bad men"
// - Lord Acton
//
extension UIImage {
    
    func pixelColor(atPoint point: CGPoint) -> (color: UIColor, isTransparent: Bool) {
        let pixelData = self.cgImage!.dataProvider!.data
        let data: UnsafePointer<UInt8> = CFDataGetBytePtr(pixelData)

        let pixelInfo: Int = ((Int(self.size.width) * Int(point.y)) + Int(point.x)) * 4
        let r = CGFloat(data[pixelInfo]) / CGFloat(255.0)
        let g = CGFloat(data[pixelInfo+1]) / CGFloat(255.0)
        let b = CGFloat(data[pixelInfo+2]) / CGFloat(255.0)
        let a = CGFloat(data[pixelInfo+3]) / CGFloat(255.0)
        
        let isTransparent = (a < 1.0) ? true : false
        return ( UIColor(red: r, green: g, blue: b, alpha: a), isTransparent)
    }
    
    func resize(to dimension: Int) -> UIImage {
        return self.resize(to: CGSize(width: dimension, height: dimension))
    }
    
    func resize(to dimension: CGFloat) -> UIImage {
        return self.resize(to: CGSize(width: dimension, height: dimension))
    }

    func resize(to newSize: CGSize) -> UIImage {
        let rect = CGRect(origin: CGPoint.zero, size: newSize)
        UIGraphicsBeginImageContextWithOptions(newSize, true, 0.0);

        self.draw(in: rect)
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!

        UIGraphicsEndImageContext()
        return newImage
    }

    func addingBackground(color: UIColor) -> UIImage {
        guard self != SupportedImages.placeholderImage else { return self }

        // Don't bother doing the background fill if the image is not transparent
        // NOTE: the getPixelColor method only checks the value of the first pixel
        let colorInfo = self.pixelColor(atPoint: CGPoint(x: 1, y: 1))
        guard colorInfo.isTransparent else { return self }
        
        let size = self.size
        let rect = CGRect(origin: CGPoint.zero, size: size)
        UIGraphicsBeginImageContextWithOptions(size, true, 0.0)
        
        let bg = UIBezierPath(rect: rect)
        color.setFill()
        bg.fill()
        
        self.draw(in: rect, blendMode: CGBlendMode.normal, alpha: 1.0)
        let newImage = UIGraphicsGetImageFromCurrentImageContext();
        
        UIGraphicsEndImageContext()
        if newImage != nil {
            return newImage!
        }
        else {
            return self
        }
    }

    func roundCorners() -> UIImage {
        let size = self.size
        let rect = CGRect(origin: CGPoint.zero, size: size)
        let radius = 5 * UIScreen.main.scale
        UIGraphicsBeginImageContext(size)

        let path = UIBezierPath(roundedRect: rect, cornerRadius: radius)
        path.addClip()
        self.draw(in: rect)
        let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!

        UIGraphicsEndImageContext()
        return newImage
    }
    
    static func overlayingText(_ text: String, inRect rect: CGRect) -> UIImage {
        let placholder = SupportedImages.placeholderImage
        let rect = rect

        let font = UIFont.preferredFont(forTextStyle: .headline)
        let colour = UIColor.white

        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .center

        let attributes = [NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: colour, NSAttributedString.Key.paragraphStyle: paragraphStyle]
        let layoutRect = rect.insetBy(dx: 5, dy: 5)
        let theString = NSAttributedString(string: text, attributes: attributes)

        let stringRect = theString.boundingRect(with: layoutRect.size, options: [.usesLineFragmentOrigin,.truncatesLastVisibleLine], context: nil)
        let offsetX = (rect.width - stringRect.width) / 2
        let offsetY = (rect.height - stringRect.height) / 2
        let adjustedRect = CGRect(x: offsetX, y: offsetY - 10, width: stringRect.width, height: stringRect.height)
        
        UIGraphicsBeginImageContextWithOptions(rect.size, true, 0.0)

        placholder.draw(in: rect)
        theString.draw(in: adjustedRect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext();

        UIGraphicsEndImageContext()
        return newImage!
    }
}


