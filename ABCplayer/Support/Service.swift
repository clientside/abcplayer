//
//  ABCplayer
//
//  Copyright © 2017 Clientside. All rights reserved.
//

import Foundation

/// A Service represents a 'Station' or 'Streaming Service'.
/// The class has the ability to request JSON which contains the current
/// and next scheduled program info and update itself with this data.
///
class Service {
    
    let id: String
    let title: String
    let synopsis: String
    let images: SupportedImages
    let timezone: TimeZone?
    let serviceLocation: String?
    let streamUrl: URL?

    var isAwaitingUpdate = false
    var hasTrackData = false
    var nextStreamUpdate: Date?
    var requestedUpdate: Date?
    var currentProgram: ScheduledProgram?
    var nextProgram: ScheduledProgram?

    init(service: ABCJSON.Service) {
        self.id = service.id
        self.title = service.title
        self.synopsis = service.miniSynopsis ?? "Live stream"
        
        if let tzName = service.timezone,
            let timezone = TimeZone(identifier: tzName) {
            self.timezone = timezone
        }
        else {
            self.timezone = nil
        }
        
        self.images = SupportedImages(title: service.title, primaryImage: service.primaryImage)
        self.serviceLocation = service.location ?? nil
        self.hasTrackData = service.hasTrackData ?? false
        // Find 'LiveStream' outlets. Assume there is either one or none.
        // Return the preferred audio URL according to the types in the order
        // defined in SupportedAudio.
        //
        let LiveStreamOutlet = service.outlets.filter {$0.type == "LiveStream" }
        if let streams = LiveStreamOutlet.first?.audioStreams,
            let preferred = SupportedAudio.preferedService(streams)
        {
            self.streamUrl = URL(string: preferred.url)
        }
        else {
            
            self.streamUrl = nil
        }

    }
}
