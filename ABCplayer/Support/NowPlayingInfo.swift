//
//  ABCplayer
//
//  Copyright © 2017 Clientside. All rights reserved.
//

import UIKit

/// NowPlayingInfo has the logic to update the
/// PlayerView UI with the correct information
///
///
struct NowPlayingInfo: Equatable {
    
    let serviceId: String
    let serviceName: String
    let serviceDescription: String
    let serviceArt: UIImage
    let programTitle: String?
    let programDescription: String?
    let programSchedule: String?
    let presenter: String?
    let presenterArt: UIImage?
    
    // Track
    private(set) var playingNow: ABCJSON.MusicalRecording? = nil
    private(set) var trackEvents: TrackEvents? = nil
    private(set) var trackTitle: String? = nil
    private(set) var artist: String? = nil
    private(set) var album: String? = nil
    private(set) var coverArt: UIImage? = nil
    private(set) var duration: TimeInterval? = nil
    private(set) var durationString: String? = nil
    
    // Info for Player UI
    var displayArt: UIImage {
        return coverArt ?? presenterArt ?? serviceArt
    }
    var albumOrServiceName: String {
        return album ?? serviceName
    }
    var artistOrPresenterOrProgram: String? {
        if let artist = artist {
            return artist
        }
        if programTitle != nil && presenter != nil {
            return "\(programTitle!) / \(presenter!)"
        }
        else {
            return presenter ?? programTitle
        }
    }
    var trackOrProgramDescriptionOrServiceDescription: String {
        return trackTitle ?? programDescription ?? serviceDescription
    }
    var otherInfo: String? {
        return durationString
    }
    
    init(service: Service, tracks: ABCJSON.Tracks? = nil) {
        serviceId = service.id
        serviceName = service.title
        serviceDescription = service.synopsis
        serviceArt = service.images.playerDisplayImage
        programTitle =  service.currentProgram?.title
        programDescription = service.currentProgram?.synopsis
        programSchedule = service.currentProgram?.schedule
        presenter = service.currentProgram?.presenter?.name
        presenterArt = service.currentProgram?.presenter?.images.artistImage
        
        if tracks != nil {
            trackEvents = TrackEvents(for: tracks!)
        }
        else {
            trackEvents = nil
        }
        
        if let track = tracks?.now, service.hasTrackData {
            playingNow = track
            trackTitle = track.recording.releases.first?.title
            artist = track.recording.artists.first?.name
            album = track.recording.title
            durationString = trackEvents?.durationString
            duration = trackEvents?.duration
            
            if let coverImage = track.recording.releases.first?.artwork.first,
                let image = SupportedImages(artistImage: coverImage).artistImage
            {
                coverArt = image
            }
            else {
                coverArt = service.images.playerDisplayImage
            }
        }
    }
    
    mutating func setNextUpdate(for date: Date) throws {
        guard trackEvents != nil else { throw ABCPLayerError.updatingTrackEventsDateFailed("Not able to set nextUpdate: TrackEvents is nil") }
        if let oldDate = trackEvents?.nextUpdate, oldDate > date {
            throw ABCPLayerError.updatingTrackEventsDateFailed("Not able to set nextUpdate: nextUpdate was nil or the New Date is before the nextUpdate")
        }
        trackEvents!.nextUpdate = date
    }
    
    static func ==(lhs: NowPlayingInfo, rhs: NowPlayingInfo) -> Bool {
        return lhs.serviceId == rhs.serviceId &&
            lhs.programTitle == rhs.programTitle &&
            lhs.playingNow == rhs.playingNow
    }
    
}

