//
//  ABCplayer
//
//  Copyright © 2017 Clientside. All rights reserved.
//

import Foundation

typealias ISO8601DateString = String
typealias TimeZoneName = String

class ABCJSON {
    
    // MARK: - json root objects
    //
    
    // live.json
    struct Stream: Decodable, CustomStringConvertible {
        let lastUpdate: ISO8601DateString?
        let nextUpdate: ISO8601DateString?
        let now: Program?
        let next: Program?
        
        private enum CodingKeys : String, CodingKey {
            case now, next
            case lastUpdate = "last_updated"
            case nextUpdate = "next_updated"
         }

        init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)

            self.nextUpdate = try container.decode(String.self, forKey: .nextUpdate)
            self.lastUpdate = try container.decode(String.self, forKey: .lastUpdate)
            self.next = try? container.decode(Program.self, forKey: .next)
            self.now = try? container.decode(Program.self, forKey: .now)
        }

        var description: String {
            return "ABCJSON.live[nextUpdate: \(String(describing: nextUpdate)), lastUpdated: \(String(describing: lastUpdate)) now: \(String(describing: now)), next: \(String(describing: next))]"
        }
    }
    
    // services.json
    struct Services: Decodable, CustomStringConvertible {
        let total: Int
        let items: [Service]
        let lastUpdate: ISO8601DateString
        
        private enum CodingKeys : String, CodingKey {
            case total
            case items
            case lastUpdate = "last_updated"
        }
        
        var description: String {
            return "ABCJSON.services[total: \(total), items: \(items), lastupdate: \(lastUpdate)]"
        }
    }
    
    // now.json
    struct Tracks: Decodable, CustomStringConvertible {
        let nextUpdate: ISO8601DateString
        let lastUpdate: ISO8601DateString
        var next: MusicalRecording?
        var now: MusicalRecording?
        var prev: MusicalRecording?
        
        init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)

            self.nextUpdate = try container.decode(String.self, forKey: .nextUpdate)
            self.lastUpdate = try container.decode(String.self, forKey: .lastUpdate)
            self.next = try? container.decode(MusicalRecording.self, forKey: .next)
            self.now = try? container.decode(MusicalRecording.self, forKey: .now)
            self.prev = try? container.decode(MusicalRecording.self, forKey: .prev)
        }
        
        
        var description: String {
            return "Whatsplaying[ nextUpdate: \(nextUpdate) now: \(now?.description  ?? String("MusicalRecording[]"))"
        }
        
        private enum CodingKeys : String, CodingKey {
            case nextUpdate = "next_updated"
            case lastUpdate = "last_updated"
            case next
            case now
            case prev
        }
        
        enum Programme {
            case type
            case empty
        }
    }

    
    // MARK: - Common to live.json and services.json
    struct PrimaryImage: Decodable {
        let title: String?
        let type : String?
        let url: String
        let width: Int?
        let height: Int?
        let sizes: [SizedImage]
    }

    struct SizedImage: Decodable {
        let url: String
        let width: Int
        let height: Int
        let aspectRatio: String
       
        private enum CodingKeys : String, CodingKey {
            case url
            case width
            case height
            case aspectRatio = "aspect_ratio"
        }
    }
    
    struct Outlet: Decodable {
        let title: String
        let type: String?
        let partOfSevice: PartOfService?
        let audioStreams: [AudioStream]

        private enum CodingKeys : String, CodingKey {
            case title, type
            case partOfSevice = "partof_service"
            case audioStreams = "audio_streams"
        }
    }
    
    struct PartOfService: Decodable {
        let serviceId: String
        let title: String
        
        private enum CodingKeys : String, CodingKey {
            case title
            case serviceId = "service_id"
        }
    }

    struct AudioStream: Decodable {
        let title: String
        let url: String
        let type:  String
    }

    // MARK: - Unique to live.json
    struct Program: Decodable {
        let series: Series
        let presenters: [Presenter]
        let exludeFromIndex: Bool
        let entity: String
        let shortSynopsis: String?
        let properties: [Property]
        let lastPublishedDate: ISO8601DateString?
        let title: String
        let primaryPublicationEvent: PrimaryPublicationEvent
        let programInfo : ProgramInfo
        let miniSynopsis: String?
        let categories: [Category]
        let mediumSynopsis: String?
        let live: [Live]
        let creatingService: CreatingService?
        
        private enum CodingKeys : String, CodingKey {
            case series, entity, properties, title, categories, live, presenters
            case exludeFromIndex = "exclude_from_index"
            case shortSynopsis = "short_synopsis"
            case lastPublishedDate = "last_published_date"
            case primaryPublicationEvent = "primary_publication_event"
            case miniSynopsis = "mini_synopsis"
            case mediumSynopsis = "medium_synopsis"
            case creatingService = "creating_service"
            case programInfo = "program"
         }

    }
    
    struct Series: Decodable {
        let title: String
    }
    
    struct Presenter: Decodable {
        let primaryImage: PrimaryImage?
        let displayName: String

        private enum CodingKeys : String, CodingKey {
            case primaryImage = "primary_image"
            case displayName = "display_name"
        }

    }
    
    struct Property: Decodable {
        let type: String
        let vale: Bool?
        let user: [String]

    }

    struct PrimaryPublicationEvent: Decodable {
        let timeZoneName: TimeZoneName?
        let start: ISO8601DateString
        let end: ISO8601DateString
      
        private enum CodingKeys : String, CodingKey {
            case start, end
            case timeZoneName = "defining_timezone"
        }
    }
    
    struct Live: Decodable {
        let duration: Int
        let start: ISO8601DateString
        let end: ISO8601DateString
        let timeZoneName: TimeZoneName?
        let versionType: String
        let outlets: [Outlet]
        

        private enum CodingKeys : String, CodingKey {
            case start, end, outlets
            case duration = "duration_seconds"
            case timeZoneName = "defining_timezone"
            case versionType = "version_type"
        }
    }

    struct ProgramInfo: Decodable {
        let title: String
        let shortSynopsis: String?
        let miniSynopsis: String?
        
        private enum CodingKeys : String, CodingKey {
            case title
            case shortSynopsis = "short_synopsis"
            case miniSynopsis = "mini_synopsis"
        }
   }
    
    struct Category: Decodable {
        let label: String
        let parentCategory: ParentCategory
        
        private enum CodingKeys : String, CodingKey {
            case label
            case parentCategory = "parent_category"
        }
  }

    struct ParentCategory: Decodable {
        let label: String
    }

    struct CreatingService: Decodable {
        let service_id: String
        let title: String
    }
    
    // MARK: - Unique to services.json
    struct Service: Decodable {
        let id: String
        let title: String
        let miniSynopsis: String?
        let primaryImage: PrimaryImage?
        let images: [OtherImage]
        let outlets: [Outlet]
        let hasTrackData: Bool?
        let timezone: TimeZoneName?
        let location: String?
        
        private enum CodingKeys : String, CodingKey {
            case title, images, outlets
            case id = "service_id"
            case hasTrackData = "has_track_data"
            case miniSynopsis = "mini_synopsis"
            case primaryImage = "primary_image"
            case timezone = "service_timezone"
            case location = "service_location"
        }
    }
    
    struct OtherImage: Decodable {
        let title: String
        let url: String
        let width: Int
        let height: Int
        let sizes: [SizedImage]
        let role: String
    }
    
    //MARK: - unique to now.json    
    struct MusicalRecording: Decodable, CustomStringConvertible {
        let playedTime : ISO8601DateString
        let serviceID : String
        let recording : RecordingDescription
        
        var description: String {
            return "MusicalRecording[\(recording.title)]"
        }
        
        private enum CodingKeys : String, CodingKey {
            case playedTime = "played_time"
            case serviceID = "service_id"
            case recording
        }
    }
 
    struct RecordingDescription: Decodable {
        let title : String
        let duration : Int
        let artists : [Artist]
        let releases : [Release]
    }
    
    struct Artist: Decodable {
        let name : String
        let artwork: [PrimaryImage]
        let type : String
    }
    
    struct Release : Decodable {
        let title : String
        let artwork: [PrimaryImage]
        let artists : [Artist]
    }
}

extension ABCJSON.MusicalRecording: Equatable {
    static func == (lhs: ABCJSON.MusicalRecording, rhs: ABCJSON.MusicalRecording) -> Bool {
        return
            lhs.recording.title == rhs.recording.title &&
                lhs.recording.artists.first?.name == rhs.recording.artists.first?.name &&
                lhs.recording.releases.first?.title == rhs.recording.releases.first?.title
    }
}

