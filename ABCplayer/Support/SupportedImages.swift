//
//  ABCplayer
//
//  Copyright © 2017 Clientside. All rights reserved.
//

import UIKit

class SupportedImages {

    static let placeholderImage = UIImage(named: "placeholder")!

    private static let imageCache = NSCache<NSString, UIImage>()
    private static let servicesDisplayImageSize = CGFloat(250 * UIScreen.main.scale)
    private static let playerDisplayImageSize = CGFloat(320 * UIScreen.main.scale)
    private static let thumbnailImageSize = CGFloat(100 * UIScreen.main.scale)

    private var masterImageString: String?
    private var defaultImage: UIImage?
    private let serviceTitle: String
    
    // Don't fetch these large images unless needed
    var masterImage: UIImage {
        if let mis = masterImageString,
            let image = cachedImageFor(url: mis) {
            return image
        }
        else {
            return SupportedImages.placeholderImage
        }
    }
    
    var playerDisplayImage: UIImage {
        guard let pi = defaultImage else {return SupportedImages.placeholderImage}
        return pi.resize(to: CGSize(width: SupportedImages.playerDisplayImageSize, height: SupportedImages.playerDisplayImageSize))
    }
    
    var servicesDisplayImage: UIImage {
        guard let pi = defaultImage else {return SupportedImages.placeholderImage}
        return pi.resize(to: CGSize(width: SupportedImages.servicesDisplayImageSize, height: SupportedImages.servicesDisplayImageSize))
    }
    
    var thumbnail: UIImage {
        guard let pi = defaultImage else {return SupportedImages.placeholderImage}
        return pi.resize(to: CGSize(width: SupportedImages.thumbnailImageSize, height: SupportedImages.thumbnailImageSize))
    }
    
    var artistImage: UIImage? {
        return defaultImage?.resize(to: CGSize(width: SupportedImages.playerDisplayImageSize, height: SupportedImages.playerDisplayImageSize))
    }

    // MARK: - Initializers

    init(artistImage: ABCJSON.PrimaryImage?) {
        self.serviceTitle = ""
        if let foundImage = self.locateImage(primaryImage: artistImage) {
            self.defaultImage = foundImage
        }
    }

    // Sets the defaultImage:
    // Get a square image larger than playerDisplayImageSize
    // If none found use the Primary Image which is usually very large and square
    // but could be any size.
    //
    init(title: String, primaryImage: ABCJSON.PrimaryImage?) {
        self.serviceTitle = title
        if let foundImage = self.locateImage(primaryImage: primaryImage) {
            self.defaultImage = foundImage
        }
    }

    // MARK: - Private methods
    private func locateImage(primaryImage: ABCJSON.PrimaryImage?) -> UIImage? {
        guard primaryImage != nil else {
            return nil
        }

        masterImageString = primaryImage!.url

        guard primaryImage!.sizes.count > 0 else {
            return cachedImageFor(url: primaryImage!.url)
        }

        let matched = primaryImage!.sizes.filter {
            $0.aspectRatio == "1x1" && $0.height >= Int(SupportedImages.playerDisplayImageSize)
        }

        if let url = matched.first?.url {
            return cachedImageFor(url: url)
        }
        else {
            return cachedImageFor(url: primaryImage!.url)
        }
    }
    
    // Get a cached image if available otherwise create one and cache it.
    // The URL is used as the Key
    //
    private func cachedImageFor(url imagePath: String) -> UIImage? {

        if let cachedImage = SupportedImages.imageCache.object(forKey: imagePath as NSString) {
            return cachedImage
            
        }
        else if let url = URL(string: imagePath),
            let data = try? Data(contentsOf: url),
            let img = UIImage(data: data)
        {
            // Check if looks corrupted
            if data.endIndex > 2000 {
                // It's not probibly corruted - write image to cache
                let imageforCache = img.addingBackground(color: UIColor.white)
                SupportedImages.imageCache.setObject(imageforCache, forKey: imagePath as NSString)
                return imageforCache
            }
            else {
                // It's probibly corruted - make an image with the service.title
                let rect = CGRect(x: 0, y: 0, width: SupportedImages.playerDisplayImageSize, height: SupportedImages.playerDisplayImageSize)
                let imageforCache = UIImage.overlayingText(self.serviceTitle, inRect: rect)
                SupportedImages.imageCache.setObject(imageforCache, forKey: imagePath as NSString)
                return imageforCache
            }
        }
        return nil
    }
    
}
