//
//  ABCplayer
//
//  Copyright © 2017 Clientside. All rights reserved.
//

import UIKit

class ScheduledProgram: Equatable {
    
    let parentServiceID: String?
    let parentServiceName: String?

    let title: String
    let startDate: Date
    let endDate: Date
    let timezone: TimeZone?
    var duration: TimeInterval {
        return  self.endDate.timeIntervalSince(self.startDate)
    }
    
    let shortSynopsis: String?
    let minSynopsis: String?
    
    let presenter: Presenter?
    struct Presenter {
        let images: SupportedImages
        let name: String
    }
    
    var synopsis: String {
        return shortSynopsis ?? minSynopsis ?? "Live stream"
    }

    var schedule: String {
        let dif = DateIntervalFormatter()
        dif.dateStyle = .none
        dif.timeStyle = .short

        // Uncomment to display schedule in the station's local time
        // if let tz = self.timezone {
        //     dif.timeZone = tz
        // }

        return dif.string(from: self.startDate, to: self.endDate)
    }
    
    required init(_ program: ABCJSON.Program?) throws {
        guard let program = program else { throw ABCPLayerError.failedToInitializeScheduledProgram("Program json missing")}
        let programEvents = program.primaryPublicationEvent
        
        self.startDate = ISO8601DateFormatter().date(from: programEvents.start)!
        self.endDate = ISO8601DateFormatter().date(from: programEvents.end)!
        self.title = program.programInfo.title
        self.shortSynopsis = program.programInfo.shortSynopsis
        self.minSynopsis = program.programInfo.miniSynopsis
        self.parentServiceID = program.creatingService?.service_id
        self.parentServiceName = program.creatingService?.title
        
        if let timezoneName = programEvents.timeZoneName, let tz = TimeZone(identifier: timezoneName) {
            self.timezone = tz
        }
        else {
            self.timezone = nil
        }
        
        if let firstPresenter = program.presenters.first {
            let supportedImages = SupportedImages(artistImage: firstPresenter.primaryImage)
            self.presenter = Presenter(images: supportedImages,  name: firstPresenter.displayName)
        }
        else {
            presenter = nil
        }
        
    }
    
    static func == (lhs: ScheduledProgram, rhs: ScheduledProgram) -> Bool {
        return
            lhs.title == rhs.title &&
                lhs.startDate == rhs.startDate &&
                lhs.endDate == rhs.endDate &&
                lhs.parentServiceID == rhs.parentServiceID
    }

}

