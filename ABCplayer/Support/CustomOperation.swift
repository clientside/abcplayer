//
//  ABCplayer
//
//  Copyright © 2017 Clientside. All rights reserved.
//

import UIKit

class CustomOperation: Operation {

    private var _finished: Bool = false
    private var _executing: Bool = false
    
    func markAsCommenced() {
        isExecuting = true
        isFinished  = false
    }

    func markAsCompleted() {
        isExecuting = false
        isFinished  = true
    }

    override var isFinished: Bool {
        get {
            return _finished
        }
        set {
            willChangeValue(forKey: "isFinished")
            _finished = newValue
            didChangeValue(forKey: "isFinished")
        }
    }

    override var isAsynchronous : Bool {
        return true
    }

    override var isExecuting: Bool {
        get {
            return _executing
        }
        set {
            willChangeValue(forKey: "isExecuting")
            _executing = newValue
            didChangeValue(forKey: "isExecuting")
        }
    }

    deinit {
        NSObject.cancelPreviousPerformRequests(withTarget: self)
    }

}
