//
//  ABCplayer
//
//  Copyright © 2017 Clientside. All rights reserved.
//

import Foundation

enum TrackUpdateOperationState {
    case unkown
    case viable
    case missingTrack
    case unviable
}

class TrackUpdateOperation: CustomOperation {

    static var calculatedTimingOffset = streamLatency

    var state = TrackUpdateOperationState.unkown

    private let service: Service
    private let minimumDelay = TimeInterval(15)


    // In the end this is what we want
    private(set) var nowPlayingInfo: NowPlayingInfo

    private let pulseInterval = Pulse.shared.pulseInterval
    private var trackUpdatingTimeout: TimeInterval {
        return ((60 / pulseInterval) * timeOutMinutes)
    }

    private var updatingCount = 0

    private var playingNow: ABCJSON.MusicalRecording! {
        didSet {
            guard !isCancelled else { return }

            guard playingNow != nil else { state = .unviable; cancel(); return }

            guard Double(updatingCount) < trackUpdatingTimeout
                else {  state = .unviable; cancel(); return }

            let newNowPlaying = NowPlayingInfo(service: service, tracks: tracks)

            // Has the recording changed?
            if oldValue == playingNow {

                // Even if the recording didn't change, the `nextUpdate` may have changed
                if let oldNextUodate = nowPlayingInfo.trackEvents?.nextUpdate,
                    let newNextUpdate = newNowPlaying.trackEvents?.nextUpdate,
                    newNextUpdate > oldNextUodate
                {
                    // Just does normal update with the new `nextUpdate`
                    complete(updatingNowPlayingInfo: newNowPlaying)
                }
                else {
                    // Nothing changed - schedule a new JSON fetch
                    updatingCount += 1
                    perform(#selector(fetchTrackJson), with: nil, afterDelay: pulseInterval)
                }
            }
            else {
                // It's a new recording
                complete(updatingNowPlayingInfo: newNowPlaying)
            }
        }
    }

    private func complete(updatingNowPlayingInfo info: NowPlayingInfo) {
        //
        // Keep adjusting the timing offset - weighing fetch requests against accuracy
        //
        if updatingCount == 0 {
            TrackUpdateOperation.calculatedTimingOffset = TrackUpdateOperation.calculatedTimingOffset * 0.75
        } else {
            TrackUpdateOperation.calculatedTimingOffset = TrackUpdateOperation.calculatedTimingOffset + (pulseInterval * Double(updatingCount))
        }

        // Limit the minimum offset
        if TrackUpdateOperation.calculatedTimingOffset < minimumDelay {
            TrackUpdateOperation.calculatedTimingOffset = minimumDelay
        }

        nowPlayingInfo = info
        markAsCompleted()
    }


    private var tracks: ABCJSON.Tracks! {
        didSet {
            guard !isCancelled else { return }
            if let now = tracks.now {
                state = .viable
                self.playingNow = now
            }
            else {
                state = .missingTrack
                self.cancel()
            }
        }
    }

    init(service: Service, playingNow: ABCJSON.MusicalRecording?) {
        self.service = service
        self.playingNow = playingNow
        self.nowPlayingInfo = NowPlayingInfo(service: service)
        super.init()
    }

    override func start() {
        guard !isCancelled else { return }
        markAsCommenced()
        fetchTrackJson()
    }

    override func cancel() {
        super.cancel()
        markAsCompleted()
    }

    @objc private func fetchTrackJson() {
        guard !isCancelled else { return }
        let endpoint = Endpoint.Now.init(serviceID: service.id)
        Endpoint.fetchJSON(url: endpoint.url, type: endpoint.type) { json in
            self.tracks = json
        }
    }
}



