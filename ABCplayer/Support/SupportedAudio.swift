//
//  ABCplayer
//
//  Copyright © 2017 Clientside. All rights reserved.
//

import Foundation

struct SupportedAudio {
    static let AAC = "AAC"
    static let MP3 = "MP3"
    static let HLS = "HLS"
    private static let preferredOrder = [HLS, AAC, MP3]
}

extension SupportedAudio {
    // Hopefuly returns an audio stream found in the order of preference
    static func preferedService(_ services: [ABCJSON.AudioStream]) -> ABCJSON.AudioStream? {
        for serviceType in SupportedAudio.preferredOrder {
            let foundService = services.filter{ $0.type == serviceType }
            if let matched = foundService.first,  matched.type == serviceType {
                return matched
            }
        }
        return nil
    }
    
}

