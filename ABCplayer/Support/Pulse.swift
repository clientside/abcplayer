//
//  Pulse.swift
//
//  Copyright © 2016 Clientside. All rights reserved.
//

import Foundation

let PulseNotificationName = NSNotification.Name("Pulse")

class Pulse {
    
    // Pulse singleton
    static let shared = Pulse()
    private weak var timer: Timer?

    let pulseInterval = 10.0
    
    var isRunning: Bool {
        if timer != nil {
            return true
        }
        else {
            return false
        }
    }

    private init() {
        start()
    }
    
    deinit {
        stop()
    }
    
    func start() {
        if timer != nil {
           return
        }
        
        timer = Timer.scheduledTimer(timeInterval: self.pulseInterval, target: self, selector:  #selector(Pulse.broadcast) , userInfo: nil, repeats: true)
        timer?.fire()
    }
    
    func stop() {
        // Timer is a weak reference so invalidation will also de-reference it
        timer?.invalidate()
    }
    
    func touch() {
        timer?.fire()
    }
    
    @objc private func broadcast() {
        NotificationCenter.default.post(name: PulseNotificationName, object: self, userInfo: ["date": Date()])
    }

}
