//
//  ABCplayer
//
//  Copyright © 2017 Clientside. All rights reserved.
//

import Foundation

struct TrackEvents {
    
    var nextUpdate: Date?
    let lastUpdate: Date?
    let timePlayed: Date?
    let scheduledEnd: Date?
    let duration: TimeInterval?
    let durationString: String?

    var laterDate: Date {
        if let end = scheduledEnd, let next = nextUpdate, end > next, next.timeIntervalSince(end) <= 10 {
            return end
        }
        else {
            return nextUpdate ?? Date().addingTimeInterval(180) // if all else fails then make it 'in 3 minutes'
        }
    }

    var timeToUpdateTrack: Date {
        return laterDate.addingTimeInterval(TrackUpdateOperation.calculatedTimingOffset)
    }

    var shouldUpdateTrack: Bool {
        let now = Date()
        let shouldUpdateTrack = timeToUpdateTrack < now
        return shouldUpdateTrack
    }
    
    init(for tracks: ABCJSON.Tracks) {
        self.nextUpdate = ISO8601DateFormatter().date(from: tracks.nextUpdate)
        self.lastUpdate = ISO8601DateFormatter().date(from: tracks.lastUpdate)
        if let now = tracks.now, let timePlayed = ISO8601DateFormatter().date(from: now.playedTime) {
            let interval = TimeInterval(now.recording.duration)
            self.timePlayed = timePlayed
            self.scheduledEnd = Date(timeInterval: interval, since: timePlayed)
            self.durationString = TrackEvents.format(duration: interval)
            self.duration = interval
        }
        else {
            self.timePlayed = nil
            self.scheduledEnd = nil
            self.durationString = nil
            self.duration = nil
        }
    }
    
   private static func format(duration: TimeInterval) -> String {
        let formatter = DateComponentsFormatter()
        formatter.allowedUnits = [ .hour, .minute, .second]
        formatter.unitsStyle = .positional
        formatter.maximumUnitCount = 3
        formatter.zeroFormattingBehavior = .dropLeading
        
        return formatter.string(from: duration)!
    }
}
