//
//  ABCplayer
//
//  Copyright © 2017 Clientside. All rights reserved.
//

import Foundation

enum ProgramUpdateOperationState {
    case unknown
    case temporary
    case deferred
    case final
}

class ProgramUpdateOperation: CustomOperation {

    private let service: Service
    
    var state = ProgramUpdateOperationState.unknown
    var delay: TimeInterval {
        if let duration = service.currentProgram?.duration {
            return duration * 0.5
        }
        return 300.0
    }

    init(service: Service) {
        self.service = service
        super.init()
    }

    override func start() {
        guard !isCancelled else { return }
        markAsCommenced()
        updateProgram()
    }

    override func cancel() {
        super.cancel()
        state = .unknown
        markAsCompleted()
    }

    private func updateProgram() {
        guard !isCancelled else { return }
        guard !service.isAwaitingUpdate  else {
            fetchProgramJson()
            return
        }

        let now = Date()

        // Often the program data isn't refreshed for sometime after the end of a program.
        // If we know the next program, then use that data and postpone fetching new data
        // till a later date
        //
        if let next = service.nextProgram,
            let current = service.currentProgram,
            now >= next.startDate,
            now >= current.endDate
        {
            service.currentProgram = next
            service.nextProgram = nil
            state = .temporary
            self.service.isAwaitingUpdate = true
            markAsCompleted()
        }
        else {
            fetchProgramJson()
        }
    }

    private func fetchProgramJson() {
        guard !isCancelled else { return }

        let endpoint = Endpoint.Live.init(serviceID: service.id)
        Endpoint.fetchJSON(url: endpoint.url, type: endpoint.type) { json in

            guard !self.isCancelled else { return }

            // Is the Current Program viable
            let newCurrentProgram = try? ScheduledProgram(json.now)
            if let new = newCurrentProgram, self.service.currentProgram == new, !self.service.isAwaitingUpdate {
                self.state = .deferred
                self.markAsCompleted()
                return
            }

            // Update
            if let nextUpdate = json.nextUpdate {
                self.service.nextStreamUpdate = ISO8601DateFormatter().date(from: nextUpdate)
            } else {
                self.service.nextStreamUpdate = nil
            }

            self.service.currentProgram = newCurrentProgram
            self.service.nextProgram = try? ScheduledProgram(json.next)

            self.state = .final
            self.markAsCompleted()
        }
    }

}
