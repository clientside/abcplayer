//
//  ABCplayer
//
//  Copyright © 2017 Clientside. All rights reserved.
//

import UIKit

enum ABCPLayerError: Error  {
    case noSupportedAudioForService(String)
    case unableToLoadAudioWithURL(String)
    case failedToInitializeScheduledProgram(String)
    case updatingTrackEventsDateFailed(String)
}

