# ABCPlayer #
ABCPlayer is an appleTV app that plays audio streams from abc.net.au for my private use.

I was sick of AirPlaying audio from the ABC's Listen app, which is iPhone/iPad only. This app is a minimal effort to play ABC audio on appleTV until such time as ABC make an official app for it.

The UI reflects this minimal effort… also I have a fast, reliable fibre internet connection and so-far haven't had to worry about having the UI cope with slow, unreliable connections -- your mileage may vary

### Data in the feeds can be tad cockamamy ###
It's especially cockamamy where a program provides playlist. I'm intrigued to know why the `now` JSON is so eccentric and erratic. I don't fully understand the API, but the ABC website seems to occasionally suffer from similar glitches and misleading data. Unfortunately there's no documentation to go on. 

The ABC API isn't 'public' and it could change at anytime without warning.

In parsing the JSON I expect some parameters (ones that would make no sense without) to be present -- so far they have been -- so the app could crash if the feed got any nuttier than usual. 

__Also I have no idea if the steams or the json endpoints are region locked.__


### Built Using ###

* tvOS 12.1
* Swift 4.2
* Xcode 10.1

If you make changes to how and when JSON requests are made then please change the User Agent string to reflect the modification.


Feel free to reach-out - [@fewenfarbetween](https://twitter.com/fewenfarbetween)
